#include "calculatrice.h"
#include "gtest/gtest.h"
//https://google.github.io/googletest/reference/assertions.html

TEST(AddTest, ValidNumbers) 
{
    ASSERT_EQ(12, add(10,2));
}

TEST(SousTest, ValidNumbers) 
{
    ASSERT_EQ(8, sous(10,2));
}

TEST(MultTest, ValidNumbers) 
{
    ASSERT_EQ(20, mult(10,2));
}

TEST(DiviTest, ValidNumbers) 
{
    ASSERT_EQ(5, divi(10,2));
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
